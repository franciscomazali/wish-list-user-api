﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace Luizalabs.WishListAPI.User.Web.API.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly Business.Interface.IUser _bus;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public UsersController(Business.Interface.IUser bus, IMapper mapper, ILogger<UsersController> logger)
        {
            _logger = logger;
            _mapper = mapper;
            _bus = bus;
        }

        // GET users/?page_size=10&page=1
        [HttpGet]
        public ActionResult<IEnumerable<Model.UserDto>> Get(int page_size = 10, int page = 1)
        {
            _logger.LogInformation($"url: users/{HttpContext.Request.QueryString.Value}");

            try
            {
                var users = _bus.GetUsers(page_size, page);
                var result = _mapper.Map<List<Model.UserDto>>(users);
                return result;
            }
            catch (AutoMapperMappingException ex)
            {
                _logger.LogError($"Automapper Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Erro ao mapear entidade",
                    code = 500
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Ocorreu um erro interno.",
                    code = 500
                });
            }
        }

        // POST users/
        [HttpPost]

        public IActionResult Post(Model.UserDto model)
        {
            _logger.LogInformation($"Request: {{email: {model.email}, name: {model.name}}}");

            try
            {
                var user = _mapper.Map<User.Model.User>(model);
                string message = _bus.Create(user);
                return StatusCode(201);
            }
            catch (AutoMapperMappingException ex)
            {
                _logger.LogError($"Automapper Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new {
                    message = "Erro ao mapear entidade",
                    code = 500
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Ocorreu um erro interno.",
                    code = 500
                });
            }
        }

        [HttpHead]
        public void Options()
        {
            Response.Headers.Add("Access-Control-Allow-Headers", "content-type");
            Response.Headers.Add("Access-Control-Allow-Methods", "GET; POST");
        }
    }
}