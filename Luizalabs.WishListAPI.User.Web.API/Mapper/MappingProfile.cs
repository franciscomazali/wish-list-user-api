﻿using AutoMapper;

namespace Luizalabs.WishListAPI.User.Web.API.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User.Model.User, Model.UserDto>();
            CreateMap<Model.UserDto, User.Model.User>();
        }
    }
}
