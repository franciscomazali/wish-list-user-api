﻿using System.ComponentModel.DataAnnotations;

namespace Luizalabs.WishListAPI.User.Web.API.Model
{
    public class UserDto
    {

        public string id { get; set; }
        [Required(ErrorMessage = "Nome é obrigatório")]
        public string name { get; set; }
        [Required(ErrorMessage = "Email é obrigatório")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email inválido")]
        public string email { get; set; }
    }
}
