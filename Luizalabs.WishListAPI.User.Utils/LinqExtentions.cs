﻿using System.Collections.Generic;
using System.Linq;

namespace Luizalabs.WishListAPI.User.Utils
{
    public static class LinqExtentions
    {
        public static IEnumerable<TSource> Page<TSource>(this IEnumerable<TSource> source, int page, int pageSize)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }
    }
}
