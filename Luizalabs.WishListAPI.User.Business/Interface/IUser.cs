﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luizalabs.WishListAPI.User.Business.Interface
{
    public interface IUser
    {
        List<Model.User> GetUsers(int page_size, int page);
        string Create(Model.User model);

    }
}
