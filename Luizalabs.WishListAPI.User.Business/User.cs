﻿using Luizalabs.WishListAPI.User.Business.Interface;
using Luizalabs.WishListAPI.User.Firebase;
using System;
using System.Collections.Generic;
using System.Linq;
using static Luizalabs.WishListAPI.User.Utils.LinqExtentions;

namespace Luizalabs.WishListAPI.User.Business
{
    public class User : IUser
    {
        public List<Model.User> GetUsers(int page_size, int page)
        {
            FireUser fire = new FireUser();
            var users = fire.GetUsers();

            if (users != null)
            {
                var result = users.Select(x =>
                {
                    return new Model.User
                    {
                        email = x.Value.email,
                        id = x.Key,
                        name = x.Value.name

                    };
                });

                result = result.Page(page, page_size).ToList();

                return result.ToList();
            }

            return new List<Model.User>();

        }

        public string Create(Model.User model)
        {
            FireUser fire = new FireUser();
            string message = fire.Push(model);

            return message;
        }
    }
}
