﻿using System;

namespace Luizalabs.WishListAPI.User.Model
{
    public class User
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }
}
