﻿using FirebaseNetAdmin;
using FirebaseNetAdmin.Configurations.ServiceAccounts;
using FirebaseNetAdmin.Firebase.Commands;
using System.Collections.Generic;

namespace Luizalabs.WishListAPI.User.Firebase
{
    public class FireUser
    {

        private readonly FirebaseAdmin client;

        public FireUser()
        {
            var credentials = new JSONServiceAccountCredentials("key/wishlistapi-luiza-firebase-adminsdk-2tnz1-73493d753c.json");
            client = new FirebaseAdmin(credentials);
        }

        public string Push(Model.User user)
        {
            var users = client.Database.Ref($"users");
            var result = users.Push(user);

            return result;
        }

        public Dictionary<string, Model.User> GetUsers()
        {
            var db = client.Database.Ref($"users");
            var users = db.Get<Dictionary<string, Model.User>>();
            
            return users;
            
        }

    }
}
